/*
 Navicat Premium Data Transfer

 Source Server         : yuedu
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : pinglun

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 07/04/2021 20:29:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for content
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 106 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO `content` VALUES (105, '++++++++++++++++++++');
INSERT INTO `content` VALUES (104, '312131203');
INSERT INTO `content` VALUES (103, '0321032131');

-- ----------------------------
-- Table structure for pros
-- ----------------------------
DROP TABLE IF EXISTS `pros`;
CREATE TABLE `pros`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `price` int(11) NULL DEFAULT NULL,
  `img` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `intro` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pros
-- ----------------------------
INSERT INTO `pros` VALUES (1, '面纱', 16, './img/1.jpg', '《面纱》讲述了：容貌娇美而又爱慕虚荣的英国女子凯蒂，为了避免自己变成一位老姑娘，接受了生性孤僻的医生瓦尔特·费恩的求婚。她离开了上世纪20年代伦敦浮华而空虚的社交圈，随瓦尔特远赴神秘的东方殖民地——香港。\r\n\r\n对婚姻感到不满和无趣，凯蒂开始悄悄与令她芳心摇动的香港助理布政司查理·唐生偷情。瓦尔特发现妻子的不忠后，孤注一掷，开始了他奇特而可怕的报复计划：凯蒂必须随他前往遥远的中国内地，去平息一场正疯狂流\r\n\r\n行的霍乱瘟疫。在异国美丽却凶险的环境中，他们经历了在英国家乡的舒适\r\n\r\n生活中无法想象和体验的情感波澜……在爱情、背叛与死亡的漩涡中挣扎的凯蒂，亲历了幻想破灭与生死离别之后，终将生活的面纱从她的眼前渐渐揭去，从此踏上了不悔的精神成长之路。');
INSERT INTO `pros` VALUES (2, '简爱', 20, './img/2.jpg', '《简爱》讲述了这样一个故事：简•爱自幼父母双亡，投靠冷酷的舅母，但舅母无情地抛弃了她。她在一所慈善学校做了六年的学生和两年的教师。十八岁时，简•爱受聘到桑菲尔德府学当家庭教师，认识了主人罗切斯特。两人都被对方独特的气质和丰富的感情所吸引，于是不顾身份和地位的巨大差距深深相爱了。正当他们举行婚礼时，有人证明罗切斯特的前妻还活着。简•爱知道他们不可能有平等的婚姻，于是选择了离开。后来，简•爱意外遇见了她的表兄妹们，并从叔叔那里继承了一笔遗产。但她无法抵御对罗切斯特的刻骨思念，于是便回到了已经失去了财富、身体也遭到火灾严重摧残的罗切斯特身边，毅然跟他结婚。在爱的沐浴下，罗切斯特找回了幸福和健康。');
INSERT INTO `pros` VALUES (3, '芒果街上的小屋', 30, './img/3.jpg', '《芒果街上的小屋》是一本优美纯净的小书，一本“诗小说”。它由几十个短篇组成，一个短篇讲述一个人、一件事、一个梦想、几朵云，几棵树、几种感觉，语言清澈如流水，点缀着零落的韵脚和新奇的譬喻，如一首首长歌短调，各自成韵，又彼此钩连，汇聚出一个清晰世界，各样杂沓人生。所有的讲述都归于一个叙述中心：居住在芝加哥拉美移民社区芒果街上的女孩埃斯佩朗莎（埃斯佩朗莎，是西班牙语里的希望）。生就对弱的同情心和对美的感觉力，她用清澈的眼打量周围的世界，用美丽稚嫩的语言讲述成长，讲述沧桑，讲述生命的美好与不易，讲述年轻的热望和梦想，梦想着有一所自己的房子，梦想着在写作中追寻自我，获得自由和帮助别人的能力');
INSERT INTO `pros` VALUES (4, '活着', 28, './img/4.jpg', '《活着》的故事发生在一个叫“白江”的普通上班族身上，他过着孤岛般的生活，与由人工智能、机器人负责运行的社会格格不入，每天除了上班，就是去李伯的小门店买个手抓饼。直到有一天，他在下楼时与一位叫何萩铭的可爱女孩相识，莫名有熟悉的感觉。');
INSERT INTO `pros` VALUES (5, '羊脂球', 30, './img/5.jpg', '莫泊桑短篇小说的代表作。写普法战争时，法国的一群贵族、政客、商人、修女等高贵者，和一个叫作羊脂球的妓女，同乘一辆马车逃离普军占区，在一关卡受阻。普鲁士军官要求同羊脂球过夜，遭到羊脂球拒绝，高贵者们也深表气愤。但马车被扣留后，高贵者们竟施展各种伎俩迫使羊脂球就范，而羊脂球最终得到的却是高贵者们的轻蔑。小说反衬鲜明，悬念迭生，引人入胜，写出了法国各阶层在占领者面前的不同态度，揭露了贵族资产阶级的自私、虚伪和无耻，赞扬了羊脂球的牺牲精神');
INSERT INTO `pros` VALUES (6, '火焰的喷泉', 31, './img/6.jpg', '《火焰的喷泉:茨维塔耶娃书信选》是二十世纪俄罗斯诗人茨维塔耶娃的书信选集。茨维塔耶娃不仅以诗歌名世，其散文及书信创作也别有异彩，在俄国散文史上堪称戛戛独造，即使置之于世界文坛，也别具一格。茨维塔耶娃的散文成就仅次于诗歌创作。她的近四十篇随笔，逾千通函件，十来篇充满睿见的文论和作家评论，都是文学史上不可多得的精品。茨维塔耶娃的散文，一如其诗，跳跃性极强，意象纷至沓来，令人应接不暇。与其说诗人在写散文，不如说是用散文写诗');
INSERT INTO `pros` VALUES (7, '古诗文', 50, './img/7.jpg', '古诗文要籍叙录（大学用书），ISBN：9787101085914，作者：金开诚，葛兆光 著');
INSERT INTO `pros` VALUES (8, '话说中国人', 42, './img/8.jpg', '《话说中国人之三教九流》介绍了儒、释、道三教和优伶、宦官、流氓、帮会、娼妓、乞丐、赌博、风水先生与算命先生、商贾等九种封建社会职业，简明扼要地阐述了中国传统三大宗教的起源、教义、重要人物及其影响；同时，描述了这九个最典型的古代职业的产生、发展、主要职业特征以及这一行当的著名人物，以客观的态度来探讨社会职业的文化内涵。编者力求通过通俗流畅的叙述语言，结合科学的体例、丰富的图片、简约的版式以及别具匠心的设计，呈现给读者一份新鲜的历史文化大餐，让读者获得更多的审美享受和想象空间');
INSERT INTO `pros` VALUES (9, '通向往昔之旅', 30, './img/9.jpg', '《通向往昔之旅》是茨威格的中短篇小说集，收文8篇，都是他著名的、艺术性和阅读性极强的作品，包括《拍卖行的奇遇》《通向往昔之旅》等名篇。其中《通向往昔之旅》是茨威格的遗作，在伦敦一家出版社的档案库里尘封数十年后，终见天日。小说讲述了出身清贫、才气过人的化学博士路德维希与他的老板的夫人之间，苦苦相恋却难以如愿的爱情故事。小说没有剧烈的戏剧性的冲突，但却有细致入微的心理上的条分缕析和精神上细腻的描绘。故事有一个你意想不到的，然而确实是入情入理的结局');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `fruits` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `price` int(11) NULL DEFAULT NULL,
  `number` int(11) NULL DEFAULT NULL,
  `img` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (87, 'lisi', NULL, '火龙果', 5, 5, 'file:///D:/2021-3-27%E8%80%83%E8%AF%95/img/huolongguo.jpg');
INSERT INTO `users` VALUES (56, 'zhangsan', '123', NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (83, 'lisi', '123', NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (84, 'lisi', NULL, '香蕉', 2, 1, 'file:///D:/2021-3-27%E8%80%83%E8%AF%95/img/banana.jpg');
INSERT INTO `users` VALUES (88, '王润达', '123', NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (98, 'zhangsan', NULL, '面纱', 16, 1, './img/1.jpg');
INSERT INTO `users` VALUES (99, 'zhangsan', NULL, '简爱', 20, 3, './img/2.jpg');
INSERT INTO `users` VALUES (100, 'zhangsan', NULL, '芒果街上的小屋', 30, 2, './img/3.jpg');

SET FOREIGN_KEY_CHECKS = 1;
